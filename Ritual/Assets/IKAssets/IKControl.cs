﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(Animator)) ]

public class IKControl : MonoBehaviour {

    protected Animator animator;

    public bool ikActive = false;
    public Transform rightHandObj = null;
    public Transform rightElbowObj = null;
    public Transform leftHandObj = null;
    public Transform leftElbowObj = null;
    public Transform lookObj = null;
    
    public bool rightFootIK = false;
    public bool leftFootIK = false;
    public Transform rightFootGoal = null;
    public Transform leftFootObj = null;

    public float weight = 1f;

    // Use this for initialization
    void Awake () {
        animator = GetComponent<Animator>();
	}
	
    //a callback for calculating IK
    void OnAnimatorIK()
    {
        if (animator)
        {
            if(rightFootIK && rightFootGoal != null)
            {
                animator.SetIKPositionWeight(AvatarIKGoal.RightFoot, 1f);
                animator.SetIKPosition(AvatarIKGoal.RightFoot, rightFootGoal.position);
                animator.SetIKRotationWeight(AvatarIKGoal.RightFoot, 1f);
                animator.SetIKRotation(AvatarIKGoal.RightFoot, rightFootGoal.rotation);

            }
            else
            {
                animator.SetIKPositionWeight(AvatarIKGoal.RightFoot, 0f);
                animator.SetIKRotationWeight(AvatarIKGoal.RightFoot, 0f);
            }

            //if the IK is active, set position and rotation towards the goal
            if (ikActive)
            {
                //set look target position
                if(lookObj!= null)
                {
                    animator.SetLookAtWeight(weight, 0.8f, 0.6f, 1.0f, 0.5f); //body weight, head weight, eye weight, clamp weight
                    animator.SetLookAtPosition(lookObj.position);
                }

                //set right hand target pos and rot
                if(rightHandObj != null)
                {
                    animator.SetIKPositionWeight(AvatarIKGoal.RightHand, weight); // translative position weighting
                    animator.SetIKRotationWeight(AvatarIKGoal.RightHand, weight); //translative rotation weighting
                    animator.SetIKPosition(AvatarIKGoal.RightHand, rightHandObj.position);
                    animator.SetIKRotation(AvatarIKGoal.RightHand, rightHandObj.rotation);

                    animator.SetIKHintPositionWeight(AvatarIKHint.RightElbow, weight);
                    animator.SetIKHintPosition(AvatarIKHint.RightElbow, rightElbowObj.position);
                    
                    animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, weight); // translative position weighting
                    animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, weight); //translative rotation weighting
                    animator.SetIKPosition(AvatarIKGoal.LeftHand, leftHandObj.position);
                    animator.SetIKRotation(AvatarIKGoal.LeftHand, leftHandObj.rotation);

                    animator.SetIKHintPositionWeight(AvatarIKHint.LeftElbow, weight);
                    animator.SetIKHintPosition(AvatarIKHint.LeftElbow, leftElbowObj.position);
                }

            }//if the IK is not active, set pos and rotation back to original animation
            else
            {
                animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 0);
                animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 0);
                animator.SetIKHintPositionWeight(AvatarIKHint.RightElbow, 0);

                animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0);
                animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 0);
                animator.SetIKHintPositionWeight(AvatarIKHint.LeftElbow, 0);
                   
                animator.SetLookAtWeight(0);
            }

        }

    }

}
