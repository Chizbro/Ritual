﻿using UnityEngine;
using System.Collections;

public class feetIKray : MonoBehaviour {

    public float footHeightOffset = 0.05f;
    public float RayDistanceMultiply = 1.1f;
    float dist;

    public IKControl master = null;

    public Transform rightFootGoal = null;
    public Transform rightFoot = null;
    public Transform rightKnee = null;

    public LayerMask mask = -1;

    Vector3 dir, hitnorm;

    // Use this for initialization
    void Awake ()
    {
        if (!master)
            master = GetComponentInParent<IKControl>();

        dir = rightFoot.position - rightKnee.position;
        dist = Vector3.Distance(rightKnee.position, rightFoot.position);
        hitnorm = Vector3.up;
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        dir =  rightFoot.position - rightKnee.position;
        Debug.DrawRay(rightKnee.position, Vector3.down, Color.yellow);
        RaycastHit hitInfo = new RaycastHit();

        if (Physics.Raycast(rightKnee.position, Vector3.down, out hitInfo, dist * RayDistanceMultiply, mask)) //Vector3.Distance(rightKnee.position, rightFoot.position) *
        {
  

            //if meshcollider, get accurate surface normal. otherwise just use hitinfo.normal
            if( !(hitInfo.collider is MeshCollider) ) // NOT meshcollider
            {
                hitnorm = hitInfo.normal;
                Debug.DrawLine(hitInfo.point, hitnorm, Color.red);
            }
            else if ( ! hitInfo.collider.GetComponent<MeshCollider>().convex ) //mesh collider and NOT CONVEX
            {
                hitnorm = InterpolateNormal(hitInfo);
                Debug.DrawRay(hitInfo.point, hitnorm, Color.blue);
            }
            else //is a convex mesh collider
            {
                hitnorm = hitInfo.normal;
                Debug.DrawLine(hitInfo.point, hitnorm, Color.green);
            }

            rightFootGoal.position = hitInfo.point;

            rightFootGoal.rotation = Quaternion.FromToRotation(rightFootGoal.up, hitnorm);
            //rightFootGoal.LookAt( Vector3.forward, hitnorm);

            master.rightFootIK = true;
        }
        else
        {
            master.rightFootIK = false;

        }
	}

    public Vector3 InterpolateNormal(RaycastHit rayhit)
    {
        MeshCollider meshCollider = rayhit.collider as MeshCollider;
        if (meshCollider == null || meshCollider.sharedMesh == null)
            return Vector3.zero;

        //Mesh mesh = meshCollider.sharedMesh;

        Mesh mesh = rayhit.collider.GetComponent<MeshFilter>().mesh;
        Vector3[] normals = mesh.normals;
        int[] triangles = mesh.triangles;
        Vector3 n0 = normals[triangles[rayhit.triangleIndex * 3 + 0]];
        Vector3 n1 = normals[triangles[rayhit.triangleIndex * 3 + 1]];
        Vector3 n2 = normals[triangles[rayhit.triangleIndex * 3 + 2]];
        Vector3 baryCenter = rayhit.barycentricCoordinate;
        Vector3 interpolatedNormal = n0 * baryCenter.x + n1 * baryCenter.y + n2 * baryCenter.z;
        interpolatedNormal = interpolatedNormal.normalized;
        Transform hitTransform = rayhit.collider.transform;
        interpolatedNormal = hitTransform.TransformDirection(interpolatedNormal);
        //Debug.DrawRay(rayhit.point, interpolatedNormal);

        return interpolatedNormal;
    }
}
