﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class ApprenticeController : NetworkBehaviour
{
    public float WalkSpeed;
    public float YawSensitivity;
    public float PitchSensitivity;

    new Camera camera;
    float[] UpdateInput = new float[4];

	void Start ()
    {
        camera = GetComponentInChildren<Camera>();
        GameObject.Find("CursorLocker").GetComponent<CursorLocker>().Lock();
	}
	
	// Update is called once per frame
	void Update ()
    {
        UpdateInput[0] = 1.25f * Input.GetAxis("MoveLeftRight");
        UpdateInput[1] = 1.5f * Input.GetAxis("MoveUpDown");
        UpdateInput[2] = 1.25f * Input.GetAxis("JoystickPitch");
        UpdateInput[3] = 1.5f * Input.GetAxis("JoystickYaw");
    }

    void Rotate()
    {
        float yRot = UpdateInput[3] * YawSensitivity;
        float xRot = UpdateInput[2] * PitchSensitivity;
        
        transform.localRotation *= Quaternion.Euler(0f, yRot, 0f);
        camera.transform.localRotation *= Quaternion.Euler(-xRot, 0f, 0f);
    }

    void Move()
    {
        // always move along the camera forward as it is the direction that it being aimed at
        Vector3 desiredMove = transform.forward * UpdateInput[1] + transform.right * UpdateInput[0];
        
        desiredMove.x *= WalkSpeed;
        desiredMove.z *= WalkSpeed;

        desiredMove *= Time.fixedDeltaTime;

        transform.position += desiredMove;
    }

    void FixedUpdate()
    {
        if (isLocalPlayer)
        {
            Rotate();
            Move();
        }
    }
}
