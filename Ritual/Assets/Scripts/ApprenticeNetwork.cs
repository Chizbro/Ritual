﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class ApprenticeNetwork : NetworkBehaviour
{
	[SyncVar] Vector3 syncPos;
	[SyncVar] Quaternion syncRot;
	[SerializeField] float lerpRate = 15;
	[SerializeField] Transform playerTransform;

    public override void OnStartLocalPlayer()
    {
        var camOffset = gameObject.transform.Find("GroundOffset/CameraOffset").transform;
        var cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        cam.transform.parent = camOffset;
        cam.transform.localPosition = new Vector3(0, 0, 0);
        cam.transform.localRotation = Quaternion.Euler(0, 0, 0);

        var holdTarget = gameObject.transform.Find("HoldTarget").transform; //
        holdTarget.transform.parent = cam.transform; // 

        base.OnStartLocalPlayer();
    }

    void FixedUpdate()
    {
		// Putting these in FixedUpdate instead reduces network stress
		if(isLocalPlayer)
        {
			SendMovement();
		}
        else
        {
			LerpMovement();
		}
	}

	void LerpMovement()
    {
		playerTransform.position = Vector3.Lerp(playerTransform.position, syncPos, Time.deltaTime * lerpRate);
		playerTransform.rotation = Quaternion.Slerp(playerTransform.rotation, syncRot, Time.deltaTime * lerpRate);
	}

	[Command] void CmdUpdatePosition(Vector3 pos, Quaternion rot)
    {
		syncPos = pos;
		syncRot = rot;
	}

	[ClientCallback] void SendMovement()
    {
		CmdUpdatePosition(playerTransform.position, playerTransform.rotation);
	}
}