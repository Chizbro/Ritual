﻿using UnityEngine;
using System.Collections;

public class MimeController : MonoBehaviour
{
    public float PlaneHeight;
    public float PlaneWidth;
    public float TurnDelta;

    public GameObject LeftHandTarget;
    public GameObject RightHandTarget;
    public GameObject ParentObject;

    private float PlaneHalfHeight;
    private float PlaneHalfWidth;

    private Vector3 LeftHandRestPosition;
    private Vector3 RightHandRestPosition;

    private float[] UpdateInput = new float[5];

	// Use this for initialization
	void Start ()
    {
        GameObject.Find("CursorLocker").GetComponent<CursorLocker>().Lock();
        PlaneHalfHeight = PlaneHeight / 2.0f;
        PlaneHalfWidth = PlaneWidth / 2.0f;
        LeftHandRestPosition = LeftHandTarget.transform.localPosition;
        RightHandRestPosition = RightHandTarget.transform.localPosition;
	}
	
	// Update is called once per frame
	void Update ()
    {
        UpdateInput[0] = 1.25f * Input.GetAxis("LeftHandIkHorizontal");
        UpdateInput[1] = 1.5f  * Input.GetAxis("LeftHandIkVertical");
        UpdateInput[2] = 1.25f * Input.GetAxis("RightHandIkHorizontal");
        UpdateInput[3] = 1.5f  * Input.GetAxis("RightHandIkVertical");
        UpdateInput[4] = Input.GetAxis("Turn");
    }

    void FixedUpdate()
    {
        Vector3 newRightTarget = RightHandRestPosition + new Vector3(PlaneHalfWidth * UpdateInput[2], PlaneHalfHeight * UpdateInput[3]);
        Vector3 newLeftTarget = LeftHandRestPosition + new Vector3(PlaneHalfWidth * UpdateInput[0], PlaneHalfHeight * UpdateInput[1]);

        Vector3 rightDirection = newRightTarget - RightHandTarget.transform.localPosition;
        Vector3 leftDirection = newLeftTarget - LeftHandTarget.transform.localPosition;

        float rightMagnitude = Vector3.Magnitude(rightDirection);
        float leftMagnitude = Vector3.Magnitude(leftDirection);

        rightDirection = Vector3.Normalize(rightDirection);
        leftDirection = Vector3.Normalize(leftDirection);

        RightHandTarget.transform.localPosition += rightDirection * UnityEngine.Mathf.Min(rightMagnitude, 0.02f);
        LeftHandTarget.transform.localPosition += leftDirection * UnityEngine.Mathf.Min(leftMagnitude, 0.02f);

        ParentObject.transform.Rotate(transform.up, TurnDelta * UpdateInput[4]);
    }
}
