﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;

public class NewRoundEventData : BaseEventData
{
    public List<string> Recipe;
    public NewRoundEventData(EventSystem eventSystem) : base(eventSystem) { }

}

public class MimeUiController : MonoBehaviour
{
    public RecipeEvaluation RecipeEvaluation;

	// Use this for initialization
	void Start ()
    {
    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    public void OnNewRound(BaseEventData eventData)
    {
        var recipeEventData = (NewRoundEventData)eventData;

        if (recipeEventData.Recipe.Count != 3)
        {
            Debug.Log("TOO MANY ITEMS"); // TODO big error idk
        }

        // TODO this is dirty and unsafe
        // set Ingredient1 image
        gameObject.transform.Find("Ingredient1").gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>(string.Format("Ingredients/{0}", recipeEventData.Recipe[0]));
        // set Ingredient2 image
        gameObject.transform.Find("Ingredient2").gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>(string.Format("Ingredients/{0}", recipeEventData.Recipe[1]));
        // set Ingredient3 image
        gameObject.transform.Find("Ingredient3").gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>(string.Format("Ingredients/{0}", recipeEventData.Recipe[2]));

        RecipeEvaluation = GameObject.Find("RecipeEvaluation").GetComponent<RecipeEvaluation>();
    }
}
