﻿using UnityEngine;
using System.Collections;
using System;

public class CursorLocker : MonoBehaviour
{
    Action nextState;

	// Use this for initialization
	void Start ()
    {
        Lock();
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if (Input.GetKeyDown(KeyCode.Escape))
        {
            nextState();
        }
	}

    public void Lock()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        nextState = UnLock;
    }

    public void UnLock()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        nextState = Lock;
    }
}
