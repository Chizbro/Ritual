﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Test : MonoBehaviour
{
    RecipeGenerator generator;
    List<string> Recipe = new List<string>();

	// Use this for initialization
	void Start ()
    {

    }
	
	// Update is called once per frame
	void Update ()
    {

	}

    public void OnGUI()
    {
        string displayString = "Recipe:\n";

        for (int i = 0; i < Recipe.Count; i++)
        {
            displayString += Recipe[i] + "\n";
        }

        RecipeReceptacle receptacle = GameObject.Find("Receptacle").GetComponent<RecipeReceptacle>();
        bool equal = receptacle.ValidateRecipe(Recipe, false);
        int difference = receptacle.RecipeDifference(Recipe);

        displayString += "\nRecipe Good: " + equal.ToString() + " (" + difference.ToString() + ")";

        GUI.contentColor = Color.black;
        GUI.Label(new Rect(10, 10, 100, 500), displayString);
    }


}
