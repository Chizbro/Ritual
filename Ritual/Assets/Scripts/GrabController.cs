﻿using UnityEngine;
using System.Collections;

public class GrabController : MonoBehaviour
{
    public Transform HoldTarget = null;
    public GameObject IKTarget = null;
    GameObject TargetObject = null;

    bool PickUpPressed;
    bool TargetHeld;

    Vector3 IKTargetRestPosition;

	// Use this for initialization
	void Start ()
    {
        IKTargetRestPosition = IKTarget.transform.localPosition;
	}
	
	// Update is called once per frame
	void Update ()
    {
        PickUpPressed = Input.GetButtonDown("PickUp");
        
        // If object has been destroyed, stop holding it
        if ((TargetObject == null || !TargetObject.activeSelf) && TargetHeld)
        {
            TargetHeld = false;
            IKTarget.transform.localPosition = IKTargetRestPosition; // move hand
        }
       
        // drop 
        if (TargetHeld && PickUpPressed)
        {
            if (TargetObject != null && TargetObject.activeSelf)
            {
                TargetHeld = false;
                TargetObject.gameObject.GetComponent<Rigidbody>().useGravity = true;
                IKTarget.transform.localPosition = IKTargetRestPosition; // move hand
            }
        }
        // pick up
        else if (!TargetHeld && PickUpPressed)
        {
            if (TargetObject != null && TargetObject.activeSelf)
            {
                TargetHeld = true;
                TargetObject.gameObject.GetComponent<Rigidbody>().useGravity = false;
            }
        }
    }

    void FixedUpdate()
    {
        // carry
        if (TargetHeld && (TargetObject != null && TargetObject.activeSelf))
        {
            TargetObject.transform.position = HoldTarget.position;
            TargetObject.transform.rotation = HoldTarget.rotation;

            IKTarget.transform.position = TargetObject.transform.position; // move hand
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if ((TargetObject == null || !TargetObject.activeSelf) && other.gameObject.CompareTag("Ingredient"))
        {
            TargetObject = other.gameObject;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (!TargetHeld && other.gameObject == TargetObject)
        {
            TargetObject = null;
        }
    }
}
