﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Networking;
using UnityEngine.UI;

public class EvaluateEventData : BaseEventData
{
    public bool TimedOut;

    public EvaluateEventData(EventSystem system, bool timedOut) : base(system)
    {
        TimedOut = timedOut;
    }
}

public class RecipeEvaluation : NetworkBehaviour
{
    public NetController NetController;
    public RecipeGenerator RecipeGenerator;
    public RecipeReceptacle RecipeReceptacle;

    [SyncVar] public SyncListString Recipe = new SyncListString();
    private List<string> RecipeCached = new List<string>();
    bool[] ingredientAdded = {false, false, false};
    private ParticleController particles;

    public EventTrigger.TriggerEvent OnNewRound;
    
    // Use this for initialization
    void Start()
    {
        NetController = GameObject.Find("NetworkManager").GetComponent<NetController>();
        particles = GameObject.Find("CauldronParticles").GetComponent<ParticleController>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void NewRound()
    {
        if (isServer)
        {
            var templist = RecipeGenerator.GetComponent<RecipeGenerator>().GenerateRecipe(3);
            SyncListFromList(templist);

            RpcSynchroniseRecipe(string.Join(",", templist.ToArray()));
        }
    }

    public void EvaluateEvent(BaseEventData eventData)
    {
        var eed = eventData as EvaluateEventData;
        
        var won = false;
        string itemAdded = RecipeReceptacle.GetLastAdded();
        
        Debug.Log("Ingredient added: " + itemAdded);

        int index = findFirstMatching(itemAdded);

        if (index == -1)
        {
            //won = false; // TODO ???
            particles.PulseSmoke(1f);
        }
        else
        {
            won = RecipeReceptacle.ValidateRecipe(RecipeCached, false);
            GameObject.Find("MimeUiCanvas").transform.Find(string.Format("Ingredient{0}", index + 1)).gameObject.GetComponent<Image>().enabled = false;
            ingredientAdded[index] = true;
            particles.PulseSpark(1f);
        }

        var text = GameObject.Find("CommonCanvas").transform.Find("RoundEndText").GetComponent<Text>();
        if (eed.TimedOut)
        {
            if (won)
            {
                text.text = "Winner!";
            }
            else
            {
                text.text = "Failure!";
            }
            GameObject.Find("CursorLocker").GetComponent<CursorLocker>().UnLock();
            Invoke("InvokeToMenu", 4.0f);
        }
        Debug.Log(RecipeReceptacle.ValidateRecipe(RecipeCached, false));
    }
    
    void InvokeToMenu()
    {
        NetController.ToMenu();
    }

    List<string> ListFromSyncList()
    {
        List<string> result = new List<string>();

        foreach (string item in Recipe)
        {
            result.Add(item);
        }

        return result;
    }

    void SyncListFromList(List<string> input)
    {
        Recipe.Clear();

        input.ForEach(item => Recipe.Add(item));
    }

    int findFirstMatching(string item)
    {
        for (int i = 0; i < 3; i++)
        {
            if (ingredientAdded[i] == false && Recipe[i] == item)
            {
                return i;
            }
        }

        return -1;
    }

    [ClientRpc] void RpcSynchroniseRecipe(string serialised)
    {
        List<string> list = new List<string>(serialised.Split(','));

        NewRoundEventData eventData = new NewRoundEventData(EventSystem.current) { Recipe = list };
        eventData.selectedObject = this.gameObject;
        OnNewRound.Invoke(eventData);
    }
}
    