﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public static class IntExtensions
{
    public static string ToMinutesSecondsString(this int seconds)
    {
        return string.Format("{0:00}:{1:00}", seconds / 60, seconds % 60);
    }
}

public class TimerUI : MonoBehaviour {
	[SerializeField] public Text timer;
	[SerializeField] int initialTime = 100;
	private int timeRemaining;

	// Use this for initialization
	void Start () {
		timeRemaining = initialTime;
	}
	
	// Update is called once per frame
	void Update () {
		timer.text = timeRemaining.ToMinutesSecondsString();
	}

	public void SetTimeRemaining(int time){
		timeRemaining = time;
	}
}
