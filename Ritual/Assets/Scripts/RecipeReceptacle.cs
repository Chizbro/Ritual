﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Networking;

public class RecipeReceptacle : NetworkBehaviour
{
    [SyncVar] SyncListString Ingredients = new SyncListString();
    public EventTrigger.TriggerEvent OnItemAdded;

	// Use this for initialization
	void Start()
    {
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ingredient"))
        {
            CmdUpdateIngredients(other.gameObject);
        }
    }
    
    public void Reset()
    {
        if (Ingredients != null)
        {
            Ingredients.Clear();
        }
    }

    /**
        <summary>Compares the given recipe to the current ingredient list</summary>
        <parameter name="recipe">The recipe to compare against</parameter>
        <parameter name="isOrdered">Whether or not order is important for comparison</parameter>
        <returns>True if the ingredients match the recipe, false otherwise</returns>
    **/
    public bool ValidateRecipe(List<string> recipe, bool isOrdered)
    {
        bool result = true;

        if (!isOrdered)
        {
            result = (RecipeDifference(recipe) == 0);
        }
        else
        {
            result = Ingredients.SequenceEqual(recipe);
        }

        return result;
    }

    /**
        <summary>Gives the difference between the given recipe and the current ingredients.</summary>
        <parameter name="recipe">The recipe to compare against</parameter>
        <returns>Number of items different between the two lists</returns>
        <remarks>This method will handle duplicates rather than treating duplicates as single elements</remarks>
    **/
    public int RecipeDifference(List<string> recipe)
    {
        Dictionary<string, int> itemCounts = new Dictionary<string, int>();
        int result = 0;

        //Add each element in the ingredients list as positive
        for (int i = 0; i < Ingredients.Count(); i++)
        {
            string key = Ingredients[i];

            if (itemCounts.ContainsKey(key))
            {
                itemCounts[key] += 1;
            }
            else
            {
                itemCounts[key] = 1;
            }
        }

        //Subtract each element in recipe list away
        for (int i = 0; i < recipe.Count(); i++)
        {
            string key = recipe[i];

            if (itemCounts.ContainsKey(key))
            {
                itemCounts[key] -= 1;
            }
            else
            {
                itemCounts[key] = -1;
            }
        }

        //Final difference is sum(abs(counts))
        foreach (string key in itemCounts.Keys)
        {
            result += UnityEngine.Mathf.Abs(itemCounts[key]);
        }

        return result;
    }

    [Command] void CmdUpdateIngredients(GameObject obj)
    {
        Ingredients.Add(obj.name);
        obj.GetComponent<NetworkIdentity>().RemoveClientAuthority(obj.GetComponent<NetworkIdentity>().clientAuthorityOwner);
        obj.SetActive(false);
        NetworkServer.UnSpawn(obj);

        EvaluateEventData eventData = new EvaluateEventData(EventSystem.current, false);
        eventData.selectedObject = this.gameObject;
        OnItemAdded.Invoke(eventData);
    }

    public string GetLastAdded()
    {
        if (Ingredients.Count > 0)
        {
            return Ingredients[Ingredients.Count - 1];
        }
        else
        {
            return "";
        }
    }
}
