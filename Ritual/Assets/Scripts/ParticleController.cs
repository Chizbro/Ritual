﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class ParticleController : NetworkBehaviour {

	[SerializeField] float interval = 1f;
	[SerializeField] bool affectX;
	[SerializeField] bool affectY;
	[SerializeField] bool affectZ;
	private float counter;
	[SyncVar] private float sparkPulse;
	[SyncVar] private float smokePulse;
	[SyncVar] private Transform windZone;
	private ParticleSystem sparks;
	private ParticleSystem.EmissionModule sparkEmission;
	private ParticleSystem smoke;
	private ParticleSystem.EmissionModule smokeEmission;

	// Use this for initialization
	void Start () {
		counter = 0f;
		sparkPulse= 0f;
		smokePulse = 0f;
		windZone = this.transform.FindChild("Wind");
		sparks = this.transform.FindChild("Sparks").transform.GetComponent<ParticleSystem>();
		smoke = this.transform.FindChild("Smoke").transform.GetComponent<ParticleSystem>();
		Debug.Log(sparks);
		Debug.Log(smoke);
		sparkEmission = sparks.emission;
		sparkEmission.enabled = false;
		smokeEmission = smoke.emission;
		smokeEmission.enabled = false;
		Debug.Log(sparks.emission.enabled);
	}
	
	// Update is called once per frame
	void Update () {
		counter += Time.deltaTime;
		if(counter >= interval){
			counter = 0f;
			windZone.rotation = GenerateRotation(windZone.rotation);
		}
		if(sparkPulse > 0){
			sparkEmission.enabled = true;
			sparkPulse -= Time.deltaTime;
			if(sparkPulse <= 0){
				sparkEmission.enabled = false;
			}
		}
		if(smokePulse > 0){
			smokeEmission.enabled = true;
			smokePulse -= Time.deltaTime;
			if(smokePulse <= 0){
				smokeEmission.enabled = false;
			}
		}
	}

	Quaternion GenerateRotation(Quaternion oldRot){
		Quaternion newRot = Random.rotation;
		if(!affectX){
			newRot.x = oldRot.x;
		}
		if(!affectY){
			newRot.y = oldRot.y;
		}
		if(!affectZ){
			newRot.z = oldRot.z;
		}
		return newRot;
	}

	public void PulseSpark(float length){
		sparkPulse = length;
	}
	public void PulseSmoke(float length){
		smokePulse = length;
	}
}
