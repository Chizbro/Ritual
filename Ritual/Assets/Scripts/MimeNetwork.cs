﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MimeNetwork : NetworkBehaviour {

	[SerializeField] Transform mimeTransform;
	// IK Targets for mime's hand
	[SerializeField] Transform leftHandTarget;
	[SerializeField] Transform rightHandTarget;

	[SyncVar] Quaternion mimeSyncRot;
	[SyncVar] Vector3 leftSyncPos;
	[SyncVar] Quaternion leftSyncRot;
	[SyncVar] Vector3 rightSyncPos;
	[SyncVar] Quaternion rightSyncRot;

	[SerializeField] float lerpRate = 15;

    public override void OnStartLocalPlayer()
    {
        var camOffset = gameObject.transform.Find("GroundOffset/CameraOffset").transform;
        var cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        cam.transform.parent = camOffset;
        cam.transform.localPosition = new Vector3(0, 0, 0);
        cam.transform.localRotation = Quaternion.Euler(0, 0, 0);

        var uiCanvas = GameObject.Find("MimeUiCanvas").GetComponent<Canvas>();
        uiCanvas.enabled = true;
        uiCanvas.worldCamera = cam;

        base.OnStartLocalPlayer();
    }

    void FixedUpdate () {
		if(isLocalPlayer){
			SendMovement();		
		}else{
			LerpMovement();
		}
	}

	void LerpMovement(){
		mimeTransform.rotation = Quaternion.Slerp(mimeTransform.rotation, mimeSyncRot, Time.deltaTime * lerpRate);

		leftHandTarget.position = Vector3.Lerp(leftHandTarget.position, leftSyncPos, Time.deltaTime * lerpRate);
		rightHandTarget.position = Vector3.Lerp(rightHandTarget.position, rightSyncPos, Time.deltaTime * lerpRate);
		leftHandTarget.rotation = Quaternion.Slerp(leftHandTarget.rotation, leftSyncRot, Time.deltaTime * lerpRate);
		rightHandTarget.rotation = Quaternion.Slerp(rightHandTarget.rotation, rightSyncRot, Time.deltaTime * lerpRate);
	}

	[Command] void CmdUpdatePosition(Quaternion mimeRot, Vector3 leftPos, Quaternion leftRot, Vector3 rightPos, Quaternion rightRot){
		mimeSyncRot = mimeRot;
		leftSyncPos = leftPos;
		rightSyncPos = rightPos;
		leftSyncRot = leftRot;
		rightSyncRot = rightRot;
	}
	[ClientCallback] void SendMovement(){
		CmdUpdatePosition(mimeTransform.rotation, leftHandTarget.position, leftHandTarget.rotation, rightHandTarget.position, rightHandTarget.rotation);
	}
}