﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class NetworkTimer : NetworkBehaviour {
    // Other objects can set or get the remaining time via SetRemaining(float) or GetRemaining()
    // Other objects can start or stop the clock via StartCounting() or StopCounting()
    // timerUI should be attached to a canvas.

    public EventTrigger.TriggerEvent OnTimedOut;

	[SerializeField] float timeLimit = 100f;
	[SerializeField] TimerUI timerUI;
	[SyncVar] private float timeRemaining;
	private bool countingDown = false;

	public void Start(){
		timeRemaining = timeLimit;
		countingDown = true;

        var commonCanvas = GameObject.Find("CommonCanvas");
        timerUI = commonCanvas.GetComponent<TimerUI>();
        timerUI.timer = commonCanvas.transform.Find("TimerText").GetComponent<Text>();

        base.OnStartLocalPlayer();
    }

    public override void OnStartServer()
    {
        
    }

	public void Update(){
		if(countingDown){
			timeRemaining -= Time.deltaTime;
			if(timeRemaining < 0){
				timeRemaining = 0;
				countingDown = false;
                OnTimedOut.Invoke(new EvaluateEventData(EventSystem.current, true));
            }
		}
		timerUI.SetTimeRemaining((int)Mathf.Floor(timeRemaining));
	}

	public void StartCounting(){
		countingDown = true;
	}
	public void StopCounting(){
		countingDown = false;
	}
	public void SetRemaining(float time){
		timeRemaining = time;
	}
	public float GetRemaining(){
		return timeRemaining;
	}
}
