﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Net;

public class NetController : NetworkManager {

	// These GameObjects also need to be added to the "Registered Spawnable Prefabs" field
	[SerializeField] GameObject mimePrefab;
	[SerializeField] GameObject apprenticePrefab;

	[SerializeField] Vector3 apprenticeSpawn;
    [SerializeField] Vector3 mimeSpawn;

    private int playerCount = 0; // Used to track whether to spawn a mime or an apprentice

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
		if (playerCount == 0)
        {
			GameObject player = (GameObject) Instantiate(mimePrefab, mimeSpawn, Quaternion.identity);
            NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
            GameObject.Find("RecipeEvaluation").GetComponent<RecipeEvaluation>().NewRound();
            Debug.Log("Created Mime");
        }
        else if (playerCount > 0)
        {
			GameObject player = (GameObject) Instantiate(apprenticePrefab, apprenticeSpawn, Quaternion.identity);
            NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
            foreach (GameObject ingredient in GameObject.FindGameObjectsWithTag("Ingredient"))
            {
                ingredient.GetComponent<NetworkIdentity>().AssignClientAuthority(conn);
            }
            GameObject.Find("Receptacle").GetComponent<NetworkIdentity>().AssignClientAuthority(conn);
            Debug.Log("Created Apprentice");
		}

		playerCount += 1;

		Debug.Log("Player count: " + playerCount);
	}

    public override void OnServerDisconnect(NetworkConnection conn)
    {
		Debug.Log("Destroying objects for disconnected player");

		NetworkServer.DestroyPlayersForConnection(conn);

		playerCount -= 1;

		Debug.Log("Player count: " + playerCount);
	}
    
    void Stop()
    {
        playerCount = 0;

        if (NetworkClient.active)
            StopClient();
        if (NetworkServer.active)
            StopServer();
    }

    public void DoStartClient()
    {
        Stop();

        var ip = GameObject.Find("Menu").transform.Find("IpAddress/Text").GetComponent<Text>().text;
        if (!CheckIp(ip))
        {
            // TODO error message
            Debug.Log("Client Bad server Ip");
            return;
        }

        networkAddress = ip;
        StartClient(); // TODO check client.connected?
        if (!NetworkClient.active)
        {
            // TODO error message
            Debug.Log("Client not active");
        }
        else
        {
            Debug.Log("Client active");
        }
    }

    public void DoStartServer()
    {
        Stop();
        StartHost(); // TODO check client.connected?
        if (!NetworkClient.active)
        {
            Debug.Log("(Server) Client Active");
        }
        else
        {
            // TODO show error message
            Debug.Log("(Server) Client not active");
        }

        if (!NetworkClient.active)
        {
            // TODO error message
            Debug.Log("Client not active");
        }
        else
        {
            Debug.Log("Client active");
        }
    }

    public void ToMenu()
    {
        Stop();
    }

    public void Exit()
    {
        NetworkManager.Shutdown();
        Application.Quit();
    }

    public bool CheckIp(string ip)
    {
        IPAddress addr = null;
        return IPAddress.TryParse(ip, out addr);
    }
}