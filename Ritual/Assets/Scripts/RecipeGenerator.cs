﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public static class ListExtensions
{
    public static T Random<T>(this IList<T> list)
    {
        return list[UnityEngine.Random.Range(0, list.Count())];
    }

    public static IList<T> Clone<T>(this IList<T> listToClone) where T : System.ICloneable
    {
        return listToClone.Select(item => (T)item.Clone()).ToList();
    }
}

public class RecipeGenerator : MonoBehaviour
{
    List<string> Ingredients;

    void Awake()
    {
        
    }

	// Use this for initialization
	void Start ()
    {
        
    }

    void LoadIngredients()
    {
        if (Ingredients == null || Ingredients.Count == 0)
        {
            Ingredients = GameObject.FindGameObjectsWithTag("Ingredient").Select(go => go.name).ToList();
        }
    }
	
    public List<string> GenerateRecipe(int maxLength)
    {
        LoadIngredients();

        var ingredientsCache = Ingredients.Clone().ToList();
        var recipe = new List<string>();
        
        while ((maxLength > 0) && (recipe.Count() < Ingredients.Count()))
        {
            var item = ingredientsCache.Random();
            recipe.Add(item);
            ingredientsCache.Remove(item);
            maxLength--;
        }

        return recipe;
    }
}
