// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:9361,x:33990,y:32498,varname:node_9361,prsc:2|normal-2948-RGB,emission-2460-OUT,custl-9380-OUT;n:type:ShaderForge.SFN_LightAttenuation,id:8068,x:32665,y:32876,varname:node_8068,prsc:2;n:type:ShaderForge.SFN_LightColor,id:3406,x:32642,y:33076,varname:node_3406,prsc:2;n:type:ShaderForge.SFN_LightVector,id:6869,x:31858,y:32654,varname:node_6869,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:9684,x:31858,y:32782,prsc:2,pt:True;n:type:ShaderForge.SFN_HalfVector,id:9471,x:31858,y:32933,varname:node_9471,prsc:2;n:type:ShaderForge.SFN_Dot,id:7782,x:32070,y:32697,cmnt:Lambert,varname:node_7782,prsc:2,dt:1|A-6869-OUT,B-9684-OUT;n:type:ShaderForge.SFN_Dot,id:3269,x:32070,y:32871,cmnt:Blinn-Phong,varname:node_3269,prsc:2,dt:1|A-9684-OUT,B-9471-OUT;n:type:ShaderForge.SFN_Multiply,id:2746,x:32462,y:32962,cmnt:Specular Contribution,varname:node_2746,prsc:2|A-7782-OUT,B-5267-OUT,C-4865-RGB;n:type:ShaderForge.SFN_Tex2d,id:851,x:32070,y:32349,ptovrint:False,ptlb:Diffuse,ptin:_Diffuse,varname:node_851,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:b892afa3d08b095419a52f2affeef999,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:1941,x:32988,y:32840,cmnt:Diffuse Contribution,varname:node_1941,prsc:2|A-3406-RGB,B-7782-OUT,C-8068-OUT;n:type:ShaderForge.SFN_Color,id:5927,x:32070,y:32534,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_5927,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Exp,id:1700,x:32070,y:33054,varname:node_1700,prsc:2,et:1|IN-9978-OUT;n:type:ShaderForge.SFN_Slider,id:5328,x:31529,y:33056,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:node_5328,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_Power,id:5267,x:32268,y:32940,varname:node_5267,prsc:2|VAL-3269-OUT,EXP-1700-OUT;n:type:ShaderForge.SFN_Multiply,id:5085,x:32959,y:33047,cmnt:Attenuate and Color,varname:node_5085,prsc:2|A-3406-RGB,B-7782-OUT;n:type:ShaderForge.SFN_ConstantLerp,id:9978,x:31858,y:33056,varname:node_9978,prsc:2,a:1,b:11|IN-5328-OUT;n:type:ShaderForge.SFN_Color,id:4865,x:32268,y:33095,ptovrint:False,ptlb:Spec Color,ptin:_SpecColor,varname:node_4865,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_AmbientLight,id:7528,x:32846,y:32476,varname:node_7528,prsc:2;n:type:ShaderForge.SFN_Multiply,id:2460,x:33026,y:32434,cmnt:Ambient Light,varname:node_2460,prsc:2|A-544-OUT,B-7528-RGB;n:type:ShaderForge.SFN_Multiply,id:544,x:32357,y:32443,cmnt:Diffuse Color,varname:node_544,prsc:2|A-851-RGB,B-5927-RGB;n:type:ShaderForge.SFN_Tex2d,id:992,x:33460,y:33249,ptovrint:False,ptlb:Ramp,ptin:_Ramp,varname:node_992,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:9275641bf993a4643884a0e1f2b68b97,ntxv:0,isnm:False|UVIN-3620-OUT;n:type:ShaderForge.SFN_Vector1,id:1650,x:33062,y:33430,varname:node_1650,prsc:2,v1:0;n:type:ShaderForge.SFN_RgbToHsv,id:7488,x:33108,y:33019,varname:node_7488,prsc:2|IN-5085-OUT;n:type:ShaderForge.SFN_Append,id:3620,x:33169,y:33297,varname:node_3620,prsc:2|A-7488-VOUT,B-1650-OUT;n:type:ShaderForge.SFN_Multiply,id:8790,x:33305,y:32798,cmnt:Ramp Meets Diffuse,varname:node_8790,prsc:2|A-1941-OUT,B-992-RGB;n:type:ShaderForge.SFN_ScreenPos,id:8377,x:33080,y:32601,varname:node_8377,prsc:2,sctp:0;n:type:ShaderForge.SFN_Tex2d,id:3147,x:33259,y:32601,ptovrint:False,ptlb:overlay,ptin:_overlay,varname:node_3147,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-8377-UVOUT;n:type:ShaderForge.SFN_Blend,id:7834,x:33509,y:32780,varname:node_7834,prsc:2,blmd:1,clmp:True|SRC-3147-RGB,DST-8790-OUT;n:type:ShaderForge.SFN_Fresnel,id:5664,x:32683,y:33249,varname:node_5664,prsc:2|NRM-9684-OUT,EXP-4528-OUT;n:type:ShaderForge.SFN_ValueProperty,id:4528,x:32413,y:33281,ptovrint:False,ptlb:RimSharpness,ptin:_RimSharpness,varname:node_4528,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Tex2d,id:2948,x:33553,y:32383,ptovrint:False,ptlb:NormalMap,ptin:_NormalMap,varname:node_2948,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:ce9a625221cb5f34ba4c83ad360b18b9,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Multiply,id:8789,x:32981,y:33229,cmnt:Prepare Fresnel,varname:node_8789,prsc:2|A-8068-OUT,B-3406-RGB,C-5664-OUT,D-6589-OUT;n:type:ShaderForge.SFN_Add,id:9380,x:33678,y:32951,cmnt:Add Fresnel,varname:node_9380,prsc:2|A-7834-OUT,B-8789-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6589,x:32440,y:33404,ptovrint:False,ptlb:RimBrightness,ptin:_RimBrightness,varname:node_6589,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;proporder:851-5927-5328-4865-992-3147-4528-2948-6589;pass:END;sub:END;*/

Shader "wizardyShader1" {
    Properties {
        _Diffuse ("Diffuse", 2D) = "white" {}
        _Color ("Color", Color) = (1,1,1,1)
        _Gloss ("Gloss", Range(0, 1)) = 0.5
        _SpecColor ("Spec Color", Color) = (1,1,1,1)
        _Ramp ("Ramp", 2D) = "white" {}
        _overlay ("overlay", 2D) = "white" {}
        _RimSharpness ("RimSharpness", Float ) = 2
        _NormalMap ("NormalMap", 2D) = "bump" {}
        _RimBrightness ("RimBrightness", Float ) = 0.5
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform float4 _Color;
            uniform sampler2D _Ramp; uniform float4 _Ramp_ST;
            uniform sampler2D _overlay; uniform float4 _overlay_ST;
            uniform float _RimSharpness;
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform float _RimBrightness;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                float4 screenPos : TEXCOORD5;
                LIGHTING_COORDS(6,7)
                UNITY_FOG_COORDS(8)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.screenPos = o.pos;
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _NormalMap_var = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(i.uv0, _NormalMap)));
                float3 normalLocal = _NormalMap_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
////// Emissive:
                float4 _Diffuse_var = tex2D(_Diffuse,TRANSFORM_TEX(i.uv0, _Diffuse));
                float3 emissive = ((_Diffuse_var.rgb*_Color.rgb)*UNITY_LIGHTMODEL_AMBIENT.rgb);
                float4 _overlay_var = tex2D(_overlay,TRANSFORM_TEX(i.screenPos.rg, _overlay));
                float node_7782 = max(0,dot(lightDirection,normalDirection)); // Lambert
                float3 node_5085 = (_LightColor0.rgb*node_7782); // Attenuate and Color
                float4 node_7488_k = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                float4 node_7488_p = lerp(float4(float4(node_5085,0.0).zy, node_7488_k.wz), float4(float4(node_5085,0.0).yz, node_7488_k.xy), step(float4(node_5085,0.0).z, float4(node_5085,0.0).y));
                float4 node_7488_q = lerp(float4(node_7488_p.xyw, float4(node_5085,0.0).x), float4(float4(node_5085,0.0).x, node_7488_p.yzx), step(node_7488_p.x, float4(node_5085,0.0).x));
                float node_7488_d = node_7488_q.x - min(node_7488_q.w, node_7488_q.y);
                float node_7488_e = 1.0e-10;
                float3 node_7488 = float3(abs(node_7488_q.z + (node_7488_q.w - node_7488_q.y) / (6.0 * node_7488_d + node_7488_e)), node_7488_d / (node_7488_q.x + node_7488_e), node_7488_q.x);;
                float2 node_3620 = float2(node_7488.b,0.0);
                float4 _Ramp_var = tex2D(_Ramp,TRANSFORM_TEX(node_3620, _Ramp));
                float node_5664 = pow(1.0-max(0,dot(normalDirection, viewDirection)),_RimSharpness);
                float3 finalColor = emissive + (saturate((_overlay_var.rgb*((_LightColor0.rgb*node_7782*attenuation)*_Ramp_var.rgb)))+(attenuation*_LightColor0.rgb*node_5664*_RimBrightness));
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform float4 _Color;
            uniform sampler2D _Ramp; uniform float4 _Ramp_ST;
            uniform sampler2D _overlay; uniform float4 _overlay_ST;
            uniform float _RimSharpness;
            uniform sampler2D _NormalMap; uniform float4 _NormalMap_ST;
            uniform float _RimBrightness;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                float4 screenPos : TEXCOORD5;
                LIGHTING_COORDS(6,7)
                UNITY_FOG_COORDS(8)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.screenPos = o.pos;
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _NormalMap_var = UnpackNormal(tex2D(_NormalMap,TRANSFORM_TEX(i.uv0, _NormalMap)));
                float3 normalLocal = _NormalMap_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float4 _overlay_var = tex2D(_overlay,TRANSFORM_TEX(i.screenPos.rg, _overlay));
                float node_7782 = max(0,dot(lightDirection,normalDirection)); // Lambert
                float3 node_5085 = (_LightColor0.rgb*node_7782); // Attenuate and Color
                float4 node_7488_k = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                float4 node_7488_p = lerp(float4(float4(node_5085,0.0).zy, node_7488_k.wz), float4(float4(node_5085,0.0).yz, node_7488_k.xy), step(float4(node_5085,0.0).z, float4(node_5085,0.0).y));
                float4 node_7488_q = lerp(float4(node_7488_p.xyw, float4(node_5085,0.0).x), float4(float4(node_5085,0.0).x, node_7488_p.yzx), step(node_7488_p.x, float4(node_5085,0.0).x));
                float node_7488_d = node_7488_q.x - min(node_7488_q.w, node_7488_q.y);
                float node_7488_e = 1.0e-10;
                float3 node_7488 = float3(abs(node_7488_q.z + (node_7488_q.w - node_7488_q.y) / (6.0 * node_7488_d + node_7488_e)), node_7488_d / (node_7488_q.x + node_7488_e), node_7488_q.x);;
                float2 node_3620 = float2(node_7488.b,0.0);
                float4 _Ramp_var = tex2D(_Ramp,TRANSFORM_TEX(node_3620, _Ramp));
                float node_5664 = pow(1.0-max(0,dot(normalDirection, viewDirection)),_RimSharpness);
                float3 finalColor = (saturate((_overlay_var.rgb*((_LightColor0.rgb*node_7782*attenuation)*_Ramp_var.rgb)))+(attenuation*_LightColor0.rgb*node_5664*_RimBrightness));
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
